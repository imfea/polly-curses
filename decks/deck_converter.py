import json

with open('worters.txt') as csv_file:
    i = 1
    for row in csv_file:
        item = row.strip().split(';')
        print('{')
        print(f'    "id": {i},')
        print(f'    "a": "{item[0]}",')
        print(f'    "b": "{item[1]}"')
        print('},')
        i += 1

