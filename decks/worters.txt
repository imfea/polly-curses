der Gedenktag, des Gedenktags, die Gedenktage;emléknap, ünnepnap
heiligen;felszentel, szentté avat
ergänzen;behelyettesít, kipótol
heutigen;ma, mával
der Soldat, des Soldaten, die Soldaten;katona
aufwachsen;felnő, kinövi magát
ebenfalls;szintén, ugyancsak
der Heimweg, des Heimweges, die Heimwege;hazamenés, hazaút
armen;szegény
der Bettler, des Bettlers, die Bettler;koldus
treffen;eltalál, előfordul
damals;akkoriban
kaum;alig, aligha, nemigen
anhaben;hord valamit, visel valamit
frieren;fázik, fagy
entsetzlich, entsetzlicher, am entsetzlichsten;szörnyű, iszonyú, rémítő
zögern;habozik, hezitál
nehmen;elfogad, felvesz, elfoglal
teilen;oszt, eloszt, megoszt
das Schwert, des Schwertes, die Schwerter;szablya
die Mitte, der Mitte, die Mitte;közép, központ
erscheinen;megjelenés, jelenség
taufen;megkeresztel, nevez
der Bischof, des Bischofs, die Bischöfe;püspök
gründen;alapít, alapot rak
das Kloster, des Klosters, die Klöster;kolostor, monostor
gewählt;választékos
weiterhin;továbbra, továbbá, ezentúl
bescheiden;szerény
unternehmen;vállalkozik valamire
weiten;szélesít, tágít, távolodik
das Reisen, des Reisens, die Reisen;utazás
erzählen;elbeszél
sterben;meghal
das Beisein, des Beiseins, -;jelenlét
der Mönch, des Mönchs, die Mönche;szerzetes, barát
begraben;eltemet, lemond valamiről
fehlen;hiányzik, elhibáz, eltéveszt
die Gans, der Gans, die Gänse;liba
der Einwohner, des Einwohners, die Einwohner;férfi lakos
gewahlen;megválasztott, kiválasztott
die Angst, der Angst, die Ängste;félelem, aggodalom
gefunden;talált
verrieten;elmond, elárulja magát
damit;hogy, azzal, ezzel, vele
gewesen;ott volt
befehlen;megparancsol
zubereitet;elkészít
Fastens;böjt
festlichen;ünnepi
verabschiedet;elbocsát, elbúcsúzik
braten;süt, megsüt, megsül
die Verantwortung, der Verantwortung, die Verantwortungen;felelősség
schnattern;fecseg, gágog, hápog
das Weihnachten, des Weihnachten, die Weihnachten;karácsony
böse;mérges, rossz, dühös, beteg
der Gänsestall;libaól
die Gründung, der Gründung, die Gründungen;alapítás, alapzat
die/der Versteck, des Verstecks, die Verstecke;rejtekhely
behandelt;feldolgozott, kezelt
gestorben;meghalt, élettelen
verabschieden;elbúcsúzik
die Löse, der Löse, die Lösen;váltságdíj
das Rätsel, des Rätsels, die Rätsel;rejtvény, talány
der Angehörige, des Angehorigen, die Angehorige;hozzátartozó (férfi)
die Hiebwaffe, der Hiebwaffe, die Hiebwaffen;vágófegyver
die Stichwaffe, der Stichwaffe, die Stichwaffen;szúrófegyver
der Griff, des Griffes, die Griffe;foggantyú
oberster;elsőbb
geistlicher;lelki
der Würdenträger, des Würdenträgers, die Würdenträger;tekintély hordozója
das Bistum, des Bistums, die Bistümer;püspökség
die Leuchte, der Leuchte, die Leuchten;lámpa
dienend;szolgáló
das Gerät, des Geräts, die Geräte;eszköz
die Nonne, der Nonne, die Nonnen;apáca
das Kleidungsstück, des Kleidungsstückes, des Kleidungsstücks;ruha, ruhadarab
gemeinsamer;közös
der Gang, des Gangs, die Gänge;folyamat
die Menschenmenge, der Menschenmenge, die Menschenmengen;tömeg, sokaság
gefiedert;tollas
der Prister;pap
das Stadttor, des Stadttors, die Stadttore;városkapu
die Rüstung, der Rüstung, die Rüstungen;fegyverkezés
die Begebenheit, der Begebenheit, die Begebenheiten;esemény
der Kaiser, des Kaisers, die Kaiser;császár
verstecken;elbújuk, elrejtőzik
der Hühnestall, des Hühnestall, die Hühneställe;tyúkól
der Papst, des Papsters, die Päpste;pápa
reich, reicher, reichsten;gazdag, bőséges, dús
bescheiden;szerény
geschprochen;beszélt
seitdem;azóta, amióat
seligen;boldogít, üdvözít
der Laternenumzug, des Laternenumzuges, die Laternenumzüge;lampionos felvonulás
der Erwachsener, des Erwachsenen, die Erwachsene;felnőtt
das Lied, des Liedes, die Lieder;dal
gleich;azonos
das Ohr, des Ohres, die Ohren;fül
der Vordergrund, des Vordergrundes, die Vordergründe;előtér
gelingen;sikerül, készül
widmen + sich;valaminek odaadja/szenteli magát
zahlreiche;számos, rengeteg
die Geschichte, der Geschichte, die Geschichten;történet, elbeszélés, monda
erzählen;elbeszél
die Güte;jóság
die Ehre, der Ehre, die Ehren;becsület
versammeln sich;gyülekezik
vielerorts;sokhelyütt
verkleidet sich;álruhába öltözik
reiten;lovalog
gefolgen;követ
bunt, bunter, am buntesten;színes, tarka
das Gericht, des Gerichts, die Gerichte;étel, fogás
weit, weiter, am weitesten;széles, nagy
verbreitet, verbreiteter, am verbreitetsten;elterjed
der Gänsebraten;libasült, libapecsenye
gerade;éppen, pontosan
weihen;felavat, felszentel
schnattern;fecseg, gágog
aufgeregt;izgatott
entdecken;felfedez, felfed
schließlich;végüli, mégis
geweiht;szentelt, megszentel
wahrscheinlich, wahrscheinlicher, am wahrscheinlichsten;valószínű
jährlich;évi, évenkénti
anfallende;előforduló, fellépő
die Lehnspflicht, der Lehnspflicht, die Lehnspflichten;hűbéri kötelesség
der Bauer, des Bauern, die Bauer;paraszt
zurückgehen;csökkent, visszamegy
fällig;esedékes
die Regel, der Regel, die Regeln;szabály, előírás
bestehen;létezik, van
die Abgabe, der Abgabe, die Agbagen;adó, illeték, hozzájárulás
leistende;teljesített
häufig, häufiger, am häufigsten;gyakran
gleich;azonos, egyenlő
festlich, festlicher, am festlichsten;ünnepi, ünnepélyes
der Braten, des Bratens, die Braten;sült, pecsenye
verspeisen;elfogyaszt, elkölt, megeszik
der Genuss, des Genusses, die Genüsse;fogyasztás (étel, ital)
dennoch;mégis
besonderer;sajátos
