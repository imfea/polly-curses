import json
import os
import random

from polly.deck import Deck

DECK_DIRECTORY = 'decks'


class Selection(object):
    """
    Represents a selection of the decks and the stabilities.
    """
    
    def __init__(self, selection_path):
        """
        Initialize a selection from a JSON file.
        :param selection_path: path of the JSON file
        """
        with open(selection_path) as selection_file:
            content = json.load(selection_file)
        self._selection_id = content['id']
        self._variability = content['variability']
        self._repeat_min_step = content['repeat_min_step']
        self._last_card_ids = []
        self.load_decks(content['decks'])
        self._stabilities = content['stabilities']
        self.check_stabilities()

    def load_decks(self, deck_ids):
        """
        Load the decks by deck identifiers.
        :param deck_ids: list of deck identifiers
        """
        self._decks = {}
        for deck_id in deck_ids:
            deck_path = os.path.join(DECK_DIRECTORY, f'{deck_id}.json')
            deck = Deck(deck_path)
            self._decks[deck_id] = deck

    def check_stabilities(self):
        """
        Check that the stabilities are valid.
        """
        # TODO: Check the missing cards!
        # TODO: Check the missing stability values!
        if self._stabilities == []:
            self.set_initial_stabilities()

    def set_initial_stabilities(self):
        """
        Set the initial stabilites for all cards to zero.
        """
        stabilities = []
        for deck_id, deck in self._decks.items():
            for card_id in deck.collect_card_ids():
                stability = [0, deck_id, card_id]
                stabilities.append(stability)
        random.shuffle(stabilities)
        self._stabilities = stabilities

    def choose_card_index(self):
        """
        Chose an index from the selected cards.
        :return: index of the chosen card
        """
        while True:
            card_index = int(random.expovariate(1.0 / self._variability))
            if card_index >= len(self._stabilities):
                card_index = len(self._stabilities) - 1
            _, deck_id, card_id = self._stabilities[card_index]
            last_card_id = (deck_id, card_id)
            if last_card_id not in self._last_card_ids:
                self._last_card_ids.append(last_card_id)
                if len(self._last_card_ids) > self._repeat_min_step:
                    del self._last_card_ids[0]
                break
        return card_index

    def get_card_by_index(self, card_index):
        """
        Get the card by its index.
        :param card_index: index of the card (ordered by stability)
        :return: a Card instance
        """
        _, deck_id, card_id = self._stabilities[card_index]
        card = self._decks[deck_id].get_card(card_id)
        return card

    def change_card_stability(self, card_index, delta):
        """
        Change the stability of the given card.
        :param card_index: index of the card (ordered by stability)
        :param delta: the measure of the stability change as an integer
        :return: None
        """
        modified = self._stabilities.pop(card_index)
        modified[0] += delta
        new_stability = modified[0]
        new_index = 0
        while new_index < len(self._stabilities) and self._stabilities[new_index][0] < new_stability:
            new_index += 1
        self._stabilities.insert(new_index, modified)
