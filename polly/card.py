class Card(object):
    """
    Represents a flashcard.
    """
    
    def __init__(self, card_id, side_a, side_b):
        """
        Initialize a card.
        :param card_id: unique identifier of the card
        :param side_a: content of the side a
        :param side_b: content of the side b
        """
        self._card_id = card_id
        self._side_a = side_a
        self._side_b = side_b

    @property
    def card_id(self):
        """
        Get the unique identifier.
        """
        return self._card_id
    
    @property
    def side_a(self):
        """
        Get the content of the side a.
        """
        return self._side_a

    @property
    def side_b(self):
        """
        Get the content of the side b.
        """
        return self._side_b
