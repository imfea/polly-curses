import json

from polly.card import Card


class Deck(object):
    """
    Represents a deck of the cards.
    """
    
    def __init__(self, deck_path):
        """
        Initialize a deck from a JSON file.
        :param deck_path: path of the JSON file
        """
        with open(deck_path) as deck_file:
            content = json.load(deck_file)
        self._deck_id = content['id']
        self._description = content['description']
        self._cards = {}
        for card_data in content['cards']:
            card_id = card_data['id']
            card = Card(card_id, card_data['a'], card_data['b'])
            self._cards[card_id] = card

    @property
    def deck_id(self):
        """
        Get the unique identifier.
        """
        return self._deck_id

    @property
    def description(self):
        """
        Get the description of the deck.
        """
        return self._description

    def get_card(self, card_id):
        """
        Get the card from the deck by id.
        """
        if card_id in self._cards:
            return self._cards[card_id]
        else:
            raise ValueError(f'The card id {card_id} is invalid!')

    def collect_card_ids(self):
        """
        Collect the card identifiers to a list.
        """
        return list(self._cards.keys())
