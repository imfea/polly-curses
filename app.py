import curses
import os
import sys

from polly.selection import Selection

SIDE_A_COLOR = 1
SIDE_B_COLOR = 2


def main(stdscr):
    curses.curs_set(False)
    curses.init_pair(SIDE_A_COLOR, 3, 4)
    curses.init_pair(SIDE_B_COLOR, 3, 1)
    stdscr.clear()
    need_new_card = True
    while True:
        if need_new_card:
            card_index = selection.choose_card_index()
            card = selection.get_card_by_index(card_index)
            need_new_card = False
            is_solution_visible = False
            need_repaint = True
        if need_repaint:
            stdscr.clear()
            stdscr.addstr(2, 4, card.side_a, curses.color_pair(SIDE_A_COLOR) | curses.A_BOLD)
            if is_solution_visible:
                stdscr.addstr(4, 4, card.side_b, curses.color_pair(SIDE_B_COLOR) | curses.A_BOLD)
            stdscr.refresh()
            need_repaint = False
        c = stdscr.getch()
        if c == ord('i'):
            is_solution_visible = not is_solution_visible
            need_repaint = True
        elif c == ord('1'):
            selection.change_card_stability(card_index, -2)
            need_new_card = True
        elif c == ord('2'):
            selection.change_card_stability(card_index, -1)
            need_new_card = True
        elif c == ord('3'):
            selection.change_card_stability(card_index, +1)
            need_new_card = True
        elif c == ord('4'):
            selection.change_card_stability(card_index, +2)
            need_new_card = True
        elif c == ord('c'):
            break
        elif c == ord('q'):
            break


if __name__ == '__main__':
    if len(sys.argv) == 2:
        selection_name = sys.argv[1]
        selection_path = os.path.join('selections', f'{selection_name}.json')
        selection = Selection(selection_path)
        curses.wrapper(main)
    else:
        print('ERROR: Invalid number of command line arguments!')
        print('Usage: app.py <selection name>')
